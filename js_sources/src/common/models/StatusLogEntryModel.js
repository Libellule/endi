import Bb from 'backbone';
import BaseModel from "base/models/BaseModel";

const StatusLogEntryModel = BaseModel.extend({
    'props': [
        'id',
        'datetime',
        'status',
        'label',
        'comment',
        'icon',
        'css_class',
        'user',
        'can_edit',
        'visibility',
        'pinned',
    ],
    defaults: {
        'label': '',
        'comment': '',
        'pinned': false,
    },
});
export default StatusLogEntryModel;

