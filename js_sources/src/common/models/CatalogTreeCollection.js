/*
 * File Name : CatalogTreeCollection.js
 */
import Bb from 'backbone';
import CatalogTreeModel from './CatalogTreeModel.js';

const CatalogTreeCollection = Bb.Collection.extend({
    model: CatalogTreeModel,
    initialize(){
        CatalogTreeCollection.__super__.initialize.apply(this, arguments);
        this.current = this.models.filter(model => model.get('selected'));
    },
    getSelected(){
        return this.current;
    },
    allSelected(){
        return this.current.length == this.length;
    },
    setSelected(model, value){
        model.set('selected', value);
        this.current.push(model);
        this.trigger('change:selected');
    },
    setAllSelected(models){
        // Set all elements selected : allow to pass a list of 
        // models (in case of all selection on filtered list)
        if (models === undefined){
            models = this.models;
        }
        this.current = models;
        this.current.forEach(item => item.set('selected', true));
        this.trigger('change:selected');
    },
    setNoneSelected(){
        console.log("Set None selected");
        this.current.forEach(item => item.set('selected', false));
        this.current = [];
        this.trigger('change:selected');
    }
});
export default CatalogTreeCollection;
