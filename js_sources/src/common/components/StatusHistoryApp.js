import Mn from 'backbone.marionette';
import StatusHistoryView from "../views/StatusHistoryView";

/** Gère l'historique des statuts/mémos sur un objet
 */
const StatusHistoryAppClass = Mn.Application.extend({
    region: {
        el: '.status_history',
        replaceElement: true,
    },
    /**
     * @param {Bb.Collection} status_history  the statuses history
     */
    onStart(app, status_history){
        const view = new StatusHistoryView({collection: status_history});
        this.showView(view);
    },
});
const StatusHistoryApp = new StatusHistoryAppClass();

export default StatusHistoryApp;
