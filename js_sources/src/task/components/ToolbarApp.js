import ValidationLimitToolbarAppClass from "common/components/ValidationLimitToolbarAppClass";
import Radio from 'backbone.radio';
import SmallResumeView from "../views/resume/SmallResumeView";


const ToolbarAppClass = ValidationLimitToolbarAppClass.extend({
    getResumeView(actions) {
        const facade = Radio.channel('facade');
        const model = facade.request('get:model', 'total');
        return new SmallResumeView({
            model: model
        });
    }
})
const ToolbarApp = new ToolbarAppClass();
export default ToolbarApp;