import _ from 'underscore';
import Mn from 'backbone.marionette';
import { formatAmount } from '../../../../../math.js';
import Radio from 'backbone.radio';

const template = require('./templates/DiscountView.mustache');

const DiscountView = Mn.View.extend({
    template: template,
    tagName: 'tr',
    ui:{
        edit_button: 'button.edit',
        delete_button: 'button.delete'
    },
    // Trigger to the parent
    triggers: {
        'click @ui.edit_button': 'edit',
        'click @ui.delete_button': 'delete'
    },
    modelEvents: {
        'change': 'render'
    },
    initialize(){
        var channel = Radio.channel('config');
        this.tva_options = channel.request('get:options', 'tvas');
    },
    getTvaLabel(){
        let res = "";
        let current_value = this.model.get('tva');
        _.each(
            this.tva_options,
            function(tva){
                if (tva.value == current_value){
                    res = tva.name;
                }
            }
        );
        return res
    },
    templateContext(){
        return {
            edit: this.getOption('edit'),
            amount_label: this.model.amount_label(),
            tva_label: this.getTvaLabel(),
        };
    }
});
export default DiscountView;
