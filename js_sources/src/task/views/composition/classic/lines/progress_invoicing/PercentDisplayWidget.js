import Mn from 'backbone.marionette';
import {getOpt} from '../../../../../../tools.js';

const template = require('./templates/PercentDisplayWidget.mustache');

const PercentDisplayWidget = Mn.View.extend({
    /*
     Widget displaying a currently edited percentage showing three values :
     done already configured
     current currently configured
     left left
    */
    tagName: 'div',
    className: 'billing_slider',
    template: template,

    ui: {"anchor": "a"},
    triggers: {
        "click @ui.anchor": "slider:clicked"
    },
    templateContext: function(){
        const ctx = {
            done: getOpt(this, 'done', 0),
            current: getOpt(this, 'current', 0),
            left: getOpt(this, 'left', 0)
        };
        return ctx;
    }
});

export default PercentDisplayWidget;