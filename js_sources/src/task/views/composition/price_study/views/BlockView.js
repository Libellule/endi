/*
 * Module name : BlockView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import PriceStudyView from "./PriceStudyView.js";

import DiscountComponent from "./discount/DiscountComponent.js";
import ErrorView from 'base/views/ErrorView.js';
import ChapterCollectionView from './chapter/ChapterCollectionView.js';

const template = require('./templates/BlockView.mustache');


const BlockView = Mn.View.extend({
    template: template,
    regions: {
        common: '.common',
        products: '.products',
        discounts: '#discounts',
        errors: '.errors',
    },
    initialize(options) {
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        this.section = options['section'];
    },
    showPriceStudy() {
        this.showChildView(
            'common',
            new PriceStudyView({
                model: this.model,
                edit: this.section['edit']
            })
        );
    },
    showChapters() {
        console.log("Show collection of chapters");
        const view = new ChapterCollectionView({
            collection: this.collection,
            childViewOptions: {
                edit: this.section['edit'],
            }
        });
        this.showChildView('products', view);
    },
    onRender() {
        this.showPriceStudy();
        this.showChapters();
        this.showDiscounts();
    },
    showDiscounts() {
        const collection = this.facade.request("get:collection", 'discounts');
        this.showChildView(
            'discounts',
            new DiscountComponent({
                collection: collection,
                editable: this.section['edit']
            })
        );
    },
    templateContext() {
        return {
            editable: this.section['edit']
        };
    },
    formOk() {
        var result = true;
        var errors = this.facade.request('is:valid');
        if (!_.isEmpty(errors)) {
            this.showChildView(
                'errors',
                new ErrorView({
                    errors: errors
                })
            );
            result = false;
        } else {
            this.detachChildView('errors');
        }
        return result;
    }
});
export default BlockView