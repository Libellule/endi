import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import _ from 'underscore';
import ChapterView from './ChapterView'

const ChapterEmptyView = Mn.View.extend({
    template: _.template('<div>Aucun élément</div>')
})
const ChapterCollectionView = Mn.CollectionView.extend({
    childView: ChapterView,
    emptyView: ChapterEmptyView,
    tagName: 'div',
    childViewTriggers: {},
    collectionEvents: {
        'change:reorder': 'render',
        'sync': 'render'
    },
    initialize() {
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
    },
});
export default ChapterCollectionView