/*
 * Module name : RootComponent
 */
import Mn from 'backbone.marionette';

import DiscountForm from './discount/DiscountForm.js';
import ProductForm from './product/ProductForm.js';
import BlockView from "./BlockView.js";
import WorkForm from './workform/WorkForm.js';
import ChapterForm from './chapter/ChapterForm.js';
import WorkItemForm from './workform/WorkItemForm.js';


const template = require('./templates/RootComponent.mustache');

const RootComponent = Mn.View.extend({
    className: '',
    template: template,
    regions: {
        main: '.main',
        modalContainer: '.modal-container',
    },
    initialize(options) {
        this.initialized = false;
        this.section = options['section'];
    },
    index() {
        this.initialized = true;
        const view = new BlockView({
            model: this.model,
            collection: this.collection,
            section: this.section,
        });
        this.showChildView('main', view);
    },
    showModal(view) {
        this.showChildView('modalContainer', view);
    },
    _showChapterForm(model, collection, edit) {
        this.index();
        const view = new ChapterForm({
            model: model,
            destCollection: collection,
            edit: edit
        });
        this.showModal(view);
    },
    showAddChapterForm(model, collection) {
        this._showChapterForm(model, collection, false);
    },
    showEditChapterForm(model, collection) {
        this._showChapterForm(model, collection, true);
    },
    _showProductForm(model, collection, edit) {
        this.index();
        console.log("_showProductForm")
        let view = new ProductForm({
            model: model,
            destCollection: collection,
            edit: edit
        });
        this.showModal(view);
    },
    showAddProductForm(model, collection) {
        this._showProductForm(model, collection, false);
    },
    showEditProductForm(model) {
        this._showProductForm(model, model.collection, true);
    },
    _showDiscountForm(model, collection, edit) {
        this.index();
        let view = new DiscountForm({
            model: model,
            destCollection: collection,
            edit: edit
        });
        this.showModal(view);
    },
    showAddDiscountForm(model, collection) {
        this._showDiscountForm(model, collection, false);
    },
    showEditDiscountForm(model) {
        this._showDiscountForm(model, model.collection, true);
    },
    _showWorkForm(model, collection, edit) {
        this.index();
        let view = new WorkForm({
            model: model,
            destCollection: collection,
            edit: edit
        });
        this.showModal(view);
    },
    showAddWorkForm(model, collection) {
        this._showWorkForm(model, collection, false);
    },
    showEditWorkForm(model) {
        this._showWorkForm(model, model.collection, true);
    },
    showAddWorkItemForm(model, collection) {
        this.index();
        let view = new WorkItemForm({
            model: model,
            destCollection: collection
        });
        this.showModal(view);
    },
    showEditWorkItemForm(model) {
        const view = new WorkItemForm({
            model: model
        });
        this.showModal(view);
    },
});
export default RootComponent