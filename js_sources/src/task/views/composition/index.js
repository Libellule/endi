import PriceStudyApp from "./price_study/App.js";

import ClassicCompositionComponent from "./classic/ClassicCompositionComponent";

export default function showCompositionComponent(region, section, totalmodel) {
    console.log(section);
    if (section['mode'] == 'price_study') {
        const subapp = new PriceStudyApp({
            region: region,
        });
        subapp.start({
            section: section,
        });

    } else {
        console.log("Build the Classic Composition component")
        const view = new ClassicCompositionComponent({
            section: section,
            totalmodel: totalmodel
        })
        region.show(view);
    }
}