import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import {
    formatAmount
} from 'math';
const template = require('./templates/Totals.mustache');
const Totals = Mn.View.extend({
    template: template,
    regions: {},
    ui: {},
    events: {},
    childViewEvents: {},
    childViewTriggers: {},
    initialize() {
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
    },
    templateContext: function () {
        let config = Radio.channel('config');
        let compute_mode = config.request('get:options', 'compute_mode');
        let is_ttc_mode = (compute_mode == 'ttc');
        let has_discounts;
        if (!is_ttc_mode) {
            has_discounts = this.model.get('ht_before_discounts') != this.model.get('ht');
        } else {
            has_discounts = this.model.get('ttc_before_discounts') != this.model.get('ttc');
        }

        return {
            is_ttc_mode: is_ttc_mode,
            ttc: formatAmount(this.model.get('ttc'), true),
            ht: formatAmount(this.model.get('ht'), true),
            ht_before: formatAmount(this.model.get('ht_before_discounts'), true),
            ttc_before: formatAmount(this.model.get('ttc_before_discounts'), true),
            tvas: this.model.tva_labels(),
            has_discounts: has_discounts,
        }
    },
});
export default Totals