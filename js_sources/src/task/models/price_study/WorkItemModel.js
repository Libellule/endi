/*
 * File Name :  WorkItemModel
 */
import BaseModel from 'base/models/BaseModel.js';
import DuplicableMixin from 'base/models/DuplicableMixin.js';
import Validation from 'backbone-validation'
import Radio from 'backbone.radio';
import {
    bindModelValidation,
    unbindModelValidation
} from 'backbone-tools';
import {
    strToFloat,
    getTvaPart
} from 'math.js';
import {
    findCurrentSelected
} from 'tools.js';


const WorkItemModel = BaseModel.extend(DuplicableMixin).extend(Validation.mixin).extend({
    props: [
        "id",
        "type_",
        "label",
        "description",
        "ht",
        "supplier_ht",
        "work_unit_quantity",
        "total_quantity",
        'quantity_inherited',
        "unity",
        "work_unit_ht",
        "total_ht",
        "margin_rate",
        "margin_rate_editable",
        'base_sale_product_id',
        "sync_catalog",
        'uptodate',
        'price_study_work_id',
        'mode',
        'order',
    ],
    inherited_props: ['margin_rate'],
    defaults() {
        let config = Radio.channel('config');
        let defaults = config.request('get:options', 'defaults');
        return {
            work_unit_quantity: 1,
            quantity_inherited: true,
            margin_rate_editable: true,
            mode: 'ht'
        }
    },
    validation: {
        description: {
            required: true,
            msg: "Veuillez saisir une description"
        },
        work_unit_quantity: {
            required: function (value, attr, computedState) {
                if (!this.get('locked')) {
                    return true;
                }
            }
        },
        unity: {
            required: true,
            msg: "Veuillez saisir une unité"
        },
        margin_rate: [{
                required: false,
                pattern: 'amount',
                msg: "Le coefficient de marge doit être un nombre, dans la limite de 5 chiffres après la virgule"
            },
            {
                range: [0, 9],
                msg: "Doit être compris entre 0 et 9"
            }
        ],
        ht: {
            required(value, attr, computedState) {
                return this.get('mode') == 'ht';
            },
            pattern: "amount",
            msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
        },
        supplier_ht: {
            required(value, attr, computedState) {
                return this.get('mode') == 'supplier_ht';
            },
            pattern: "amount",
            msg: "Veuillez saisir un coût d'achat, dans la limite de 5 chiffres après la virgule",
        },
    },
    initialize: function () {
        BaseModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('priceStudyFacade');
        this.config = Radio.channel('config');
        this.user_prefs = Radio.channel('user_preferences');
    },
    ht() {
        /* Return the ht value of this entry */
        return strToFloat(this.get('total_ht'));
    },
    isFromParent(attribute) {
        /*
         * Check if the given attribute's value comes from the parent ProductWork
         */
        let result;
        const editable_key = attribute + '_editable';
        if (!this.inherited_props.includes(attribute)) {
            result = false;
        } else if (this.has(editable_key) && !this.get(editable_key)) {
            result = true;
        } else {
            result = false;
        }
        return result;
    },
    supplier_ht_mode() {
        if (_.isNull(this.get('supplier_ht')) || !this.has('supplier_ht') || this.get('supplier_ht') == 0) {
            return false;
        } else {
            return true;
        }
    },
    validateModel() {
        bindModelValidation(this);
        const result = this.validate();
        unbindModelValidation(this);
        return result;
    },
});
export default WorkItemModel;