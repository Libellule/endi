/* global AppOption; */
import $ from 'jquery';

import { applicationStartup } from '../backbone-tools.js';
import StatusHistoryApp from "../common/components/StatusHistoryApp";
import Mn from "backbone.marionette";
import FacadeModelApiMixin from "../base/components/FacadeModelApiMixin";
import {ajax_call, hideLoader} from "../tools";
import StatusLogEntryCollection
    from "../common/models/StatusLogEntryCollection";


/** Kindof facade stub
 *
 * This view is not a full JS view, but we use common mechanic, in the idea
 * that it will become one sooner or later.
 */
const FacadeClass = Mn.Object.extend(FacadeModelApiMixin).extend({
    setup(options){
        this.app_options = _.clone(options);
    },
    initialize(options) {
        this.models = {};
        this.collections = {};
    },
    setupModels(context_datas) {
        this.collections['status_history'] = new StatusLogEntryCollection(
            context_datas['status_history']
        );
    },
    start() {
        const url = this.app_options.context_url;
        return ajax_call(url).then(this.setupModels.bind(this));
    },
    syncModel() {},
});


$(function(){
    // We have no main app (python-handled)
    applicationStartup(
        AppOption, null, new FacadeClass(),
        {statusHistoryApp: StatusHistoryApp}
    );
    hideLoader();
});
