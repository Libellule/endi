/*
 * File Name :  WorkItemModel
 */
import BaseModel from 'base/models/BaseModel.js';
import Radio from 'backbone.radio';
import {
    formatAmount
} from 'math.js';

const WorkItemModel = BaseModel.extend({
    props: [
        "id",
        "type_",
        "label",
        "description",
        "ht",
        "ht_editable",
        "supplier_ht",
        "supplier_ht_editable",
        "quantity",
        "unity",
        "unity_editable",
        'base_sale_product_id',
        "sync_catalog",
        'locked',
        'mode',
        "total_ht",
    ],
    defaults() {
        return {
            quantity: 1,
            locked: true,
            mode: "supplier_ht"
        }
    },
    validation: {
        'label': {
            required: true,
            msg: "Veuillez saisir un nom"
        },
        description: {
            required: true,
            msg: "Veuillez saisir une description"
        },
        type_: {
            required: true,
            msg: "Veuillez choisir un type de produit"
        }
    },
    icons: {
        sale_product_product: 'box',
        sale_product_material: 'box',
        sale_product_composite: 'product-composite',
        sale_product_training: 'chalkboard-teacher',
        sale_product_work_force: "user",
        sale_product_service_delivery: "hands-helping",
    },
    initialize: function () {
        BaseModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('facade');
    },
    supplier_ht_label() {
        if (this.get('mode') == 'supplier_ht') {
            return formatAmount(this.get('supplier_ht'), false, false);
        } else {
            return null;
        }
    },
    ht_label() {
        return formatAmount(this.get('ht'), false, false);
    },
    total_ht_label() {
        return formatAmount(this.get('total_ht'), false, false);
    },
    getIcon() {
        if (_.has(this.icons, this.get('type_'))) {
            return this.icons[this.get('type_')];
        } else {
            return false;
        }
    },
    isFromParent(attribute) {
        /*
         * Check if the given attribute's value comes from the parent ProductWork
         */
        if (this.get(attribute + '_editable')) {
            return false;
        }
        if (!this.collection) {
            return false;
        }
        let parent = this.collection._parent;
        if (!parent) {
            throw "this._parent has not been set on the WorkItem model's collection we can't answer if it's from parent !!";
        }
        let result = false;
        if (Boolean(parent.get(attribute))) {
            result = true;
        }
        return result;
    },
    isFromCatalog(attribute) {
        /*
         * Check if the given attribute comes from the catalog
         * Not editable
         */
        let result = false;
        if (this.get('locked')) {
            let editable_key = attribute + '_editable';
            if (this.has(editable_key))
                if (!this.get(editable_key) && !this.isFromParent(attribute)) {
                    result = true;
                }
        }
        return result;
    }
});
export default WorkItemModel;