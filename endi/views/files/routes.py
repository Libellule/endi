import os

from endi.views import API_ROUTE


FILES = "/files"
FILE_ITEM = os.path.join(FILES, "{id}")
FILE_PNG_ITEM = os.path.join(FILES, "{id}.png")

FILE_ITEM_API = os.path.join(API_ROUTE, "files", "{id}")
# Default public route. Access restrictions can be
# added via the object's _acl property.
PUBLIC_ITEM = "/public/{name}"


def includeme(config):
    """
    Add module's related routes
    """
    config.add_route(FILE_PNG_ITEM, FILE_PNG_ITEM, traverse="/files/{id}")
    config.add_route(FILE_ITEM, FILE_ITEM, traverse="/files/{id}")
    config.add_route(PUBLIC_ITEM, PUBLIC_ITEM, traverse="/configfiles/{name}")
    config.add_route(FILE_ITEM_API, FILE_ITEM_API, traverse="/files/{id}")
