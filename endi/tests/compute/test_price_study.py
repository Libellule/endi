import pytest

from endi.compute.price_study import (
    ProductSupplierHtComputer,
    ProductHtComputer,
    WorkItemHtComputer,
    WorkItemSupplierHtComputer,
)


class TestProductHtMode:
    @pytest.fixture
    def computer(self, mk_price_study_product, tva20):
        p = mk_price_study_product(
            supplier_ht=5000000,
            ht=1000000,
            mode="ht",
            tva=tva20,
        )
        return ProductHtComputer(p)

    def test_unit_ht(self, computer):
        assert computer.unit_ht() == 1000000


class TestWorkItemHtMode:
    @pytest.fixture
    def computer(self, mk_price_study_work_item, tva20):
        p = mk_price_study_work_item(
            supplier_ht=5000000,
            ht=1000000,
            mode="ht",
        )
        p.price_study_work.tva = tva20
        return WorkItemHtComputer(p)

    def test_unit_ht(self, computer):
        assert computer.unit_ht() == 1000000

    def test_unit_ttc(self, computer):
        assert computer.unit_ttc() == 1200000


class TestProductSupplierHtMode:
    @pytest.fixture
    def computer(self, price_study, mk_price_study_product, tva20):
        price_study.general_overhead = 0.11
        p = mk_price_study_product(
            supplier_ht=1000000,
            margin_rate=0.12,
            tva=tva20,
            mode="supplier_ht",
        )
        return ProductSupplierHtComputer(p)

    def test_flat_cost(self, computer):
        assert computer.flat_cost() == 1000000
        computer.product.supplier_ht = None
        assert computer.flat_cost() == 0

    def test_cost_price(self, computer, price_study):
        assert computer.cost_price() == 1110000
        price_study.general_overhead = 0
        assert computer.cost_price() == 1000000

    def test_intermediate_price(self, computer):
        assert int(computer.intermediate_price()) == 1261363
        computer.product.margin_rate = None
        assert int(computer.intermediate_price()) == 1110000

    def test_unit_ht(self, computer, company):
        assert int(computer.unit_ht()) == 1261363

        # Avec contribution :
        company.contribution = 10
        assert int(computer.unit_ht()) == 1401515


class TestWorkItemSupplierHtMode:
    @pytest.fixture
    def computer(self, price_study, mk_price_study_work_item, tva20):
        price_study.general_overhead = 0.11
        p = mk_price_study_work_item(
            supplier_ht=1000000,
            _margin_rate=0.12,
            mode="supplier_ht",
            work_unit_quantity=12,
            total_quantity=5,
        )
        p.price_study_work.tva = tva20
        return WorkItemSupplierHtComputer(p)

    def test_flat_cost(self, computer):
        assert computer.flat_cost() == 1000000
        computer.product.supplier_ht = None
        assert computer.flat_cost() == 0

    def test_cost_price(self, computer, price_study):
        assert computer.cost_price() == 1110000
        price_study.general_overhead = 0
        assert computer.cost_price() == 1000000

    def test_intermediate_price(self, computer):
        assert int(computer.intermediate_price()) == 1261363
        computer.product.margin_rate = None
        assert int(computer.intermediate_price()) == 1110000

    def test_unit_ht(self, computer, company):
        assert int(computer.unit_ht()) == 1261363

        # Avec contribution :
        company.contribution = 10
        assert int(computer.unit_ht()) == 1401515

    def test_unit_ttc(self, computer):
        assert int(computer.unit_ttc()) == 1513636
