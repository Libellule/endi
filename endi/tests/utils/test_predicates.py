import time
import pytest
from hashlib import md5
from endi.tests.tools import Dummy


@pytest.fixture
def settings():
    return {}


@pytest.fixture
def request_(settings):
    # request is a reserved py.test name
    registry = Dummy(settings=settings)
    req = Dummy(registry=registry, headers={})
    return req


def test_settings_has_value(request_, settings):
    from endi.utils.predicates import SettingHasValuePredicate

    predicate = SettingHasValuePredicate(("key", True), None)
    assert predicate(None, request_) is False
    predicate = SettingHasValuePredicate(("key", False), None)
    assert predicate(None, request_) is True

    settings["key"] = "Test value"
    predicate = SettingHasValuePredicate(("key", True), None)
    assert predicate(None, request_) is True
    predicate = SettingHasValuePredicate(("key", False), None)
    assert predicate(None, request_) is False


def test_get_timestamp_from_request(request_):
    from endi.utils.predicates import get_timestamp_from_request

    with pytest.raises(KeyError):
        get_timestamp_from_request(request_)

    request_.headers = {"timestamp": "124545.152"}
    assert get_timestamp_from_request(request_) == "124545.152"

    request_.headers = {"Timestamp": "124545.152"}
    assert get_timestamp_from_request(request_) == "124545.152"


def test_check_timestamp():
    from endi.utils.predicates import check_timestamp

    assert check_timestamp(time.time(), tolerance=2)
    assert not check_timestamp(time.time() - 3, tolerance=2)


def test_get_clientsecret_from_request(request_):
    from endi.utils.predicates import get_clientsecret_from_request

    with pytest.raises(KeyError):
        get_clientsecret_from_request(request_)

    with pytest.raises(ValueError):
        request_.headers = {"Authorization": "HMAC-OTHER secret"}
        get_clientsecret_from_request(request_)

    with pytest.raises(KeyError):
        request_.headers = {"Authorization": "nospacesecret"}
        get_clientsecret_from_request(request_)

    request_.headers = {"Authorization": "HMAC-MD5 secret"}
    assert get_clientsecret_from_request(request_) == "secret"

    request_.headers = {"authorization": "HMAC-MD5 secret"}
    assert get_clientsecret_from_request(request_) == "secret"


def test_check_secret():
    from endi.utils.predicates import check_secret

    # In [8]: md5('123456-secret').hexdigest()
    # Out[8]: '06dda91136f6ad4688cdf6c8fd991696'
    assert check_secret("06dda91136f6ad4688cdf6c8fd991696", 123456, "secret")


def test_api_key_authentication(request_, settings):
    from endi.utils.predicates import ApiKeyAuthenticationPredicate

    settings["key"] = "secret"
    timestamp = request_.headers["timestamp"] = time.time()
    secret_str = "%s-secret" % timestamp
    secret_bstr = secret_str.encode("utf-8")
    request_.headers["Authorization"] = "HMAC-MD5 " + md5(secret_bstr).hexdigest()

    api = ApiKeyAuthenticationPredicate("key", None)
    assert api(None, request_)

    api = ApiKeyAuthenticationPredicate("wrongkey", None)
    with pytest.raises(Exception):
        api(None, request_)
