"""
Models related to work item management

WorkItem
"""
import logging
from sqlalchemy.orm import (
    relationship,
)
from sqlalchemy import (
    Column,
    Integer,
    BigInteger,
    ForeignKey,
    Text,
    String,
    Numeric,
    Boolean,
)
from sqlalchemy.ext.hybrid import hybrid_property
from endi_base.models.base import DBBASE, default_table_args
from endi.compute.math_utils import integer_to_amount
from .sale_product import (
    SaleProductProduct,
    SaleProductWorkForce,
    SaleProductMaterial,
    SaleProductServiceDelivery,
)
from .services import WorkItemService


logger = logging.getLogger(__name__)


class WorkItem(DBBASE):
    """
    id

    Ventes

        type_
        ht (product ht)
        total_ht (ht * quantity)
        unity
        quantity
        base_sale_product_id (relationship)
        work_id (relationshio)
        product_id (relationship)
        tva_id (relationship)
        description
    """

    __table_args__ = default_table_args
    __tablename__ = "sale_catalog_work_item"

    id = Column(Integer, primary_key=True)
    type_ = Column("type_", String(30), nullable=False)
    # Indique si le work item hérite directement des informations de son
    # base_sale_product associé
    locked = Column(Boolean(), default=True)
    # Mode de calcul ht / supplier_ht
    mode = Column(String(20), default="ht", nullable=False)
    # Fields that can be locally overriden if locked is False
    _supplier_ht = Column(BigInteger())
    _ht = Column(BigInteger())
    _unity = Column(
        String(100),
        info={"colanderalchemy": {"title": "Unité"}},
    )

    # Specific fields
    description = Column(
        Text(),
        info={"colanderalchemy": {"title": "Description"}},
    )
    quantity = Column(
        Numeric(15, 5, asdecimal=False),
        info={"colanderalchemy": {"title": "Quantité"}},
        default=1,
    )
    # ht total part for this item
    total_ht = Column(BigInteger(), default=0)
    base_sale_product_id = Column(
        Integer, ForeignKey("base_sale_product.id"), nullable=False
    )
    sale_product_work_id = Column(
        Integer, ForeignKey("sale_product_work.id", ondelete="CASCADE"), nullable=False
    )

    # Relationships
    base_sale_product = relationship(
        "BaseSaleProduct",
        foreign_keys=[base_sale_product_id],
        info={"colanderalchemy": {"exclude": True}},
    )
    sale_product_work = relationship(
        "SaleProductWork",
        foreign_keys=[sale_product_work_id],
        info={"colanderalchemy": {"exclude": True}},
    )

    _endi_service = WorkItemService

    SALE_PRODUCT_FACTORIES = {
        "sale_product_product": SaleProductProduct,
        "sale_product_material": SaleProductMaterial,
        "sale_product_work_force": SaleProductWorkForce,
        "sale_product_service_delivery": SaleProductServiceDelivery,
    }

    def get_company(self):
        return getattr(self.sale_product_work, "company", None)

    def get_tva(self):
        return getattr(self.sale_product_work, "tva", None)

    def generate_sale_product(self, label, category_id, company_id, **attributes):
        """
        Generate a sale product from a work item object

        :param str label: The label configured by the user when creating the
        workitem
        :param category_id: the category
        :param company_id: the company we're working on
        :param dict attributes: The submitted datas (are not all set at this
        stage)
        :return: BasSaleProduct Object
        """
        result = self.SALE_PRODUCT_FACTORIES[self.type_]()

        result.label = label
        result.category_id = category_id
        result.company_id = company_id

        for key in (
            "description",
            "supplier_ht",
            "ht",
            "unity",
            "mode",
        ):

            if key in attributes:
                value = attributes[key]
            else:
                value = getattr(self, key, None)

            if value:
                setattr(result, key, value)
        return result

    def sync_base_sale_product(self):
        """
        Synchronize associated base_sale_product with the current work items
        info

        :return: BasSaleProduct Object
        """
        base_sale_product = self.base_sale_product
        if base_sale_product is None:
            raise Exception(
                "We try to synchronize a work item with a "
                "non-existing base_sale_product"
            )

        logger.debug("Syncing base product, ht : %s" % self.ht)

        base_sale_product.description = self.description

        # On synchronise le ht que si il n'a pas été calculé : si le coût HT
        # n'est pas renseigné
        if self._supplier_ht in (None, 0):
            if self._ht not in (None, 0):
                base_sale_product.ht = self._ht

        keys_to_sync = (
            "supplier_ht",
            "unity",
            "mode",
        )
        changes = {}
        for key in keys_to_sync:
            value = getattr(self, key)
            if value is not None:
                changes[key] = value
                setattr(base_sale_product, key, value)
                # Après avoir transféré les infos vers le catalogue, on vide le
                # champs si on est en mode "lié"
                if self.locked:
                    setattr(self, "_{}".format(key), None)

        base_sale_product.on_before_commit("update", changes)

        return base_sale_product

    @classmethod
    def from_base_sale_product(cls, sale_product):
        """
        Create a new instance generated from the given sale_product
        """
        result = cls()
        result.type_ = sale_product.type_
        result.base_sale_product_id = sale_product.id
        result.base_sale_product = sale_product
        result.description = sale_product.description
        result.mode = sale_product.mode
        result.locked = True
        return result

    # Properties forwarding the values to the parent sale_product
    @hybrid_property
    def ht(self):
        # Si le produit a un ht et que l'on a pas de supplier_ht on renvoie le
        # ht
        # Sinon le ht doit être calculé depuis les informations supplier_ht du
        # produit et depuis le produit composé
        base_product = self.base_sale_product
        if self.locked and base_product.mode == "ht" and base_product.ht:
            return base_product.ht
        else:
            return self._ht

    @ht.setter
    def ht(self, value):
        logger.debug("Trying to set the HT value to %s" % value)
        if self.ht_editable:
            logger.debug("This one is editable")
            self._ht = value
        else:
            logger.debug("This one is not editable")

    @property
    def ht_editable(self):
        """
        Check if the ht field is editable
        """
        result = True
        if self.locked and (
            self.base_sale_product.supplier_ht or self.base_sale_product.ht
        ):
            result = False
        return result

    @hybrid_property
    def supplier_ht(self):
        # Si le produit a supplier_ht ou qu'il a un ht, on considère qu'il est
        # le référent pour cette valeur
        if not self.locked:
            return self._supplier_ht
        else:
            base_product = self.base_sale_product
            if base_product.mode == "supplier_ht" and base_product.supplier_ht:
                return base_product.supplier_ht
            elif base_product.mode == "ht" and base_product.ht:
                return base_product.ht
            else:
                return self._supplier_ht

    @supplier_ht.setter
    def supplier_ht(self, value):
        logger.debug("Trying to set the supplier_ht value to %s" % value)
        if self.supplier_ht_editable:
            logger.debug("This one is editable")
            self._supplier_ht = value
        else:
            logger.debug("This one is not editable")

    @property
    def supplier_ht_editable(self):
        """
        Check if the supplier ht fields can be edited
        """
        result = True
        base_product = self.base_sale_product
        if self.locked and (base_product.supplier_ht or base_product.ht):
            result = False
        return result

    @hybrid_property
    def unity(self):
        if self.locked and self.base_sale_product.unity:
            return self.base_sale_product.unity
        else:
            return self._unity

    @unity.setter
    def unity(self, value):
        if self.unity_editable:
            self._unity = value

    @property
    def unity_editable(self):
        """
        Check if the unity is editable
        if not locked or sale_tva_work has no unity or
        base_sale_product has no unity
        """
        result = True
        if self.locked and self.base_sale_product.unity:
            result = False
        return result

    def __json__(self, request):
        return dict(
            id=self.id,
            label=self.base_sale_product.label,
            type_=self.type_,
            supplier_ht=integer_to_amount(self.supplier_ht, 5, None),
            supplier_ht_editable=self.supplier_ht_editable,
            ht=integer_to_amount(self.ht, 5, None),
            ht_editable=self.ht_editable,
            total_ht=integer_to_amount(self.total_ht, 5, None),
            unity=self.unity,
            unity_editable=self.unity_editable,
            quantity=self.quantity,
            base_sale_product_id=self.base_sale_product_id,
            sale_product_work_id=self.sale_product_work_id,
            description=self.description,
            locked=self.locked,
            mode=self.mode,
        )

    def flat_cost(self, unitary=False):
        return self._endi_service.flat_cost(self, unitary)

    def cost_price(self, unitary=False):
        return self._endi_service.cost_price(self, unitary)

    def intermediate_price(self, unitary=False):
        return self._endi_service.intermediate_price(self, unitary)

    def unit_ht(self, contribution=None):
        return self._endi_service.unit_ht(self, contribution)

    def compute_total_ht(self, contribution=None):
        return self._endi_service.compute_total_ht(self, contribution)

    def total_ttc(self, tva=None):
        return self._endi_service.total_ttc(self, tva)

    def sync_amounts(self, work=None):
        return self._endi_service.sync_amounts(self, work=None)

    def on_before_commit(self, state, changes=None):
        self._endi_service.on_before_commit(self, state, changes=changes)

    def duplicate(self):
        result = self.__class__()
        result.base_sale_product_id = self.base_sale_product_id
        result.type_ = self.type_
        result.locked = self.locked
        result.description = self.description
        result.quantity = self.quantity
        result.total_ht = self.total_ht
        result._ht = self._ht
        result._supplier_ht = self._supplier_ht
        result._unity = self._unity
        result.mode = self.mode
        return result
