"""
Models related to price study work item management

PriceStudyWorkItem
"""
import logging
from endi_base.models.base import DBBASE, default_table_args
from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    BigInteger,
    Numeric,
    ForeignKey,
    Text,
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property

from endi.compute.math_utils import integer_to_amount
from .services import PriceStudyWorkItemService

logger = logging.getLogger(__name__)


class PriceStudyWorkItem(DBBASE):
    """
    Work item

    Can be locked to the PriceStudyWork for quantity definition

    Has an uptodate boolean indicating whether the work item is synced with the
    original associated catalog item
    """

    __table_args__ = default_table_args
    __tablename__ = "price_study_work_item"

    id = Column(Integer, primary_key=True)
    order = Column(Integer, default=0)
    type_ = Column("type_", String(30), nullable=True)
    # Mode de calcul ht / supplier_ht
    mode = Column(String(20), default="ht", nullable=False)
    # Coût unitaire
    supplier_ht = Column(BigInteger(), default=0)
    # HT unitaire
    ht = Column(BigInteger(), default=0)
    # Coefficients : peuvent-être supplantés par ceux du produit composé
    # cf hybrid_property ci-dessous
    _margin_rate = Column(Numeric(6, 5, asdecimal=False))
    # Cet élément est-il à jour avec le catalogue ?
    uptodate = Column(Boolean(), default=True)

    unity = Column(
        String(100),
        info={"colanderalchemy": {"title": "Unité"}},
    )
    # Quantité par unité de produit composé
    work_unit_quantity = Column(Numeric(15, 5, asdecimal=False), default=1)
    # Quantité globale au sein du produit composé: synchonisé lors de la modification
    # des quantités du produit composé parent
    total_quantity = Column(Numeric(15, 5, asdecimal=False), default=1)
    quantity_inherited = Column(Boolean(), default=True)
    # Specific fields
    description = Column(Text())

    # HT par unité de produit composé
    work_unit_ht = Column(BigInteger(), default=0)
    # total ht au sein du produit composé
    total_ht = Column(BigInteger(), default=0)

    # FKs
    work_item_id = Column(ForeignKey("sale_catalog_work_item.id"))
    base_sale_product_id = Column(ForeignKey("base_sale_product.id"))
    price_study_work_id = Column(
        Integer, ForeignKey("price_study_work.id", ondelete="CASCADE")
    )
    # Relationships
    work_item = relationship("WorkItem")
    base_sale_product = relationship("BaseSaleProduct")
    price_study_work = relationship(
        "PriceStudyWork",
        foreign_keys=[price_study_work_id],
        info={"colanderalchemy": {"exclude": True}},
        back_populates="items",
    )

    _endi_service = PriceStudyWorkItemService

    @hybrid_property
    def margin_rate(self):
        if self.price_study_work and self.price_study_work.margin_rate:
            return self.price_study_work.margin_rate
        else:
            return self._margin_rate

    @margin_rate.setter
    def margin_rate(self, value):
        if self.margin_rate_editable:
            self._margin_rate = value

    @property
    def margin_rate_editable(self):
        """
        Check if the general overhead is editable
        """
        result = True
        if self.price_study_work and self.price_study_work.margin_rate:
            result = False
        return result

    def __json__(self, request):
        return dict(
            id=self.id,
            supplier_ht=integer_to_amount(self.supplier_ht, 5, None),
            ht=integer_to_amount(self.ht, 5, 0),
            work_unit_ht=integer_to_amount(self.work_unit_ht, 5, 0),
            total_ht=integer_to_amount(self.total_ht, 5, 0),
            unity=self.unity,
            work_item_id=self.work_item_id,
            base_sale_product_id=self.base_sale_product_id,
            price_study_work_id=self.price_study_work_id,
            description=self.description,
            margin_rate=self.margin_rate,
            margin_rate_editable=self.margin_rate_editable,
            uptodate=self.uptodate,
            work_unit_quantity=self.work_unit_quantity,
            total_quantity=self.total_quantity,
            quantity_inherited=self.quantity_inherited,
            mode=self.mode,
            order=self.order,
        )

    @classmethod
    def from_work_item(cls, price_study_work, catalog_work_item):
        """
        Load a price study work item from a catalog one
        """
        instance = cls()
        instance.price_study_work = price_study_work
        instance.work_item_id = catalog_work_item.id

        for field in (
            "description",
            "supplier_ht",
            "ht",
            "unity",
            "total_ht",
            "mode",
        ):
            setattr(instance, field, getattr(catalog_work_item, field, None))

        instance.work_unit_quantity = catalog_work_item.quantity
        instance.total_quantity = catalog_work_item.quantity
        instance.work_unit_ht = catalog_work_item.total_ht
        return instance

    @classmethod
    def from_sale_product(cls, catalog_sale_product):
        """
        Load a price study work item from a catalog product
        """
        instance = cls(work_unit_quantity=1)
        instance.base_sale_product_id = catalog_sale_product.id

        instance.supplier_ht = getattr(catalog_sale_product, "supplier_ht", None)

        for field in (
            "ht",
            "description",
            "unity",
            "mode",
        ):
            setattr(instance, field, getattr(catalog_sale_product, field, None))

        instance.work_unit_ht = catalog_sale_product.ht

        return instance

    def duplicate(self, from_parent=False):
        instance = self.__class__()

        for field in (
            "description",
            "total_ht",
            "supplier_ht",
            "margin_rate",
            "ht",
            "unity",
            "work_unit_quantity",
            "total_quantity",
            "quantity_inheritedwork_item_id",
            "work_unit_ht",
            "mode",
        ):
            setattr(instance, field, getattr(self, field, None))

        if not from_parent:
            instance.price_study_work_id = self.price_study_work_id
        return instance

    def get_company_id(self):
        return self._endi_service.get_company_id(self)

    def get_company(self):
        return self._endi_service.get_company(self)

    def get_tva(self):
        return self._endi_service.get_tva(self)

    def get_general_overhead(self):
        result = None
        if self.price_study_work:
            result = self.price_study_work.get_general_overhead()
        return result

    # Computing tools
    def flat_cost(self, unitary=False, work_level=False):
        return self._endi_service.flat_cost(self, unitary, work_level)

    def cost_price(self, unitary=False):
        return self._endi_service.cost_price(self, unitary)

    def intermediate_price(self, unitary=False):
        return self._endi_service.intermediate_price(self, unitary)

    def unit_ht(self, contribution=None):
        return self._endi_service.unit_ht(self, contribution)

    def compute_work_unit_ht(self, contribution=None):
        return self._endi_service.compute_work_unit_ht(self, contribution)

    def compute_total_ht(self, contribution=None):
        return self._endi_service.compute_total_ht(self, contribution)

    def compute_total_tva(self, contribution=None):
        return self._endi_service.compute_total_tva(self, contribution)

    def ht_by_tva(self):
        return self._endi_service.ht_by_tva(self)

    def ttc(self, contribution=None):
        return self._endi_service.ttc(self, contribution)

    def sync_amounts(self, work=None):
        """
        Sync the computed amounts

        :param obj work: The parent price_study_work
        """
        return self._endi_service.sync_amounts(self, work=None)

    def sync_quantities(self, work=None):
        """
        Sync the work_quantities

        :param obj work: The parent price_study_work
        """
        return self._endi_service.sync_quantities(self, work)

    def on_before_commit(self, request, state, changed_attrs=None):
        return self._endi_service.on_before_commit(request, self, state, changed_attrs)

    def get_task(self):
        return self.price_study_work.chapter.price_study.task
