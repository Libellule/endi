from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    BigInteger,
    Boolean,
    Text,
    Numeric,
)
from sqlalchemy.orm import (
    relationship,
)

from endi_base.models.base import (
    DBBASE,
    default_table_args,
)
from endi_base.models.mixins import TimeStampedMixin
from endi.compute.math_utils import integer_to_amount
from .services import BasePriceStudyProductService


class BasePriceStudyProduct(DBBASE, TimeStampedMixin):
    """
    Base class for PriceStudyProducts and PriceStudyWorks
    """

    __tablename__ = "base_price_study_product"
    __table_args__ = default_table_args
    __mapper_args__ = {
        "polymorphic_on": "type_",
        "polymorphic_identity": __tablename__,
    }
    id = Column(
        Integer,
        primary_key=True,
    )
    type_ = Column("type_", String(30), nullable=False)
    general_overhead = Column(Numeric(6, 5, asdecimal=False))
    margin_rate = Column(Numeric(6, 5, asdecimal=False))

    # ce produit est-il à jour avec le catalogue produit ?
    uptodate = Column(Boolean(), default=True)

    # Coût unitaire
    ht = Column(BigInteger(), default=0)

    description = Column(Text())
    unity = Column(
        String(100),
        info={"colanderalchemy": {"title": "Unité"}},
    )
    quantity = Column(
        Numeric(15, 5, asdecimal=False),
        info={"colanderalchemy": {"title": "Quantité"}},
        default=1,
    )
    total_ht = Column(BigInteger(), default=0)

    order = Column(Integer, default=0)

    # FKs
    chapter_id = Column(
        ForeignKey("price_study_chapter.id", ondelete="CASCADE"), nullable=False
    )
    product_id = Column(Integer, ForeignKey("product.id"))
    tva_id = Column(Integer, ForeignKey("tva.id"))
    task_line_id = Column(ForeignKey("task_line.id", ondelete="SET NULL"))
    # Relationships
    chapter = relationship(
        "PriceStudyChapter",
        primaryjoin="PriceStudyChapter.id==BasePriceStudyProduct.chapter_id",
        back_populates="products",
    )
    tva = relationship("Tva")
    product = relationship("Product")
    task_line = relationship(
        "TaskLine", back_populates="price_study_product", cascade="all, delete"
    )

    # view_only Relationship
    price_study = relationship(
        "PriceStudy",
        uselist=False,
        secondary="price_study_chapter",
        primaryjoin="PriceStudyChapter.id==BasePriceStudyProduct.chapter_id",
        secondaryjoin="PriceStudyChapter.price_study_id==PriceStudy.id",
        viewonly=True,
        back_populates="products",
    )

    TYPE_LABELS = {
        "price_study_work": "Ouvrage",
        "price_study_product": "Produit simple",
    }
    _endi_service = BasePriceStudyProductService

    def __json__(self, request):
        return dict(
            id=self.id,
            chapter_id=self.chapter_id,
            type_=self.type_,
            uptodate=self.uptodate,
            margin_rate=self.margin_rate,
            general_overhead=self.general_overhead,
            ht=integer_to_amount(self.ht, 5, 0),
            description=self.description,
            product_id=self.product_id,
            tva_id=self.tva_id,
            unity=self.unity,
            quantity=self.quantity,
            total_ht=integer_to_amount(self.total_ht, 5),
            order=self.order,
        )

    @classmethod
    def from_sale_product(cls, sale_product):
        instance = cls(quantity=1)
        for field in (
            "ht",
            "description",
            "unity",
        ):
            setattr(instance, field, getattr(sale_product, field, None))

        if sale_product.company and sale_product.company.margin_rate:
            instance.margin_rate = sale_product.company.margin_rate

        return instance

    def duplicate(self, from_parent=False):
        instance = self.__class__()
        for field in (
            "uptodate",
            "ht",
            "description",
            "product_id",
            "tva_id",
            "unity",
            "quantity",
            "total_ht",
            "chapter_id",
            "mode",
        ):
            setattr(instance, field, getattr(self, field, None))

        if not from_parent:
            instance.chapter_id = self.chapter_id

        company = self.get_company()
        if company and company.margin_rate:
            margin_rate = company.margin_rate
        else:
            margin_rate = self.margin_rate
        instance.margin_rate = margin_rate

        return instance

    def get_company_id(self):
        return self._endi_service.get_company_id(self)

    def get_company(self):
        return self._endi_service.get_company(self)

    def get_general_overhead(self):
        result = None
        if self.chapter:
            result = self.chapter.get_general_overhead()
        return result

    # Computing tools
    def flat_cost(self):
        return self._endi_service.flat_cost(self)

    def cost_price(self):
        return self._endi_service.cost_price(self)

    def intermediate_price(self):
        return self._endi_service.intermediate_price(self)

    def unit_ht(self, contribution=None):
        return self._endi_service.unit_ht(self, contribution)

    def compute_total_ht(self, contribution=None):
        return self._endi_service.compute_total_ht(self, contribution)

    def ht_by_tva(self):
        return self._endi_service.ht_by_tva(self)

    def ttc(self, contribution=None):
        return self._endi_service.ttc(self, contribution)

    def sync_amounts(self, propagate=True):
        """
        Set cached amounts on the current object

        :param bool propagate: Should we propagate the syncing up (else we sync down in
        case of PriceStudyWork entries) ?
        """
        return self._endi_service.sync_amounts(self, propagate=propagate)

    def on_before_commit(self, request, state, attributes=None):
        return self._endi_service.on_before_commit(request, self, state, attributes)

    def get_task(self):
        return self.chapter.price_study.task

    def sync_with_task(self, request):
        return self._endi_service.sync_with_task(request, self, self.chapter)
