"""
Models related to price study product management

PriceStudyProduct
"""
from endi_base.models.base import default_table_args
from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    BigInteger,
    String,
)
from sqlalchemy.orm import relationship

from endi.compute.math_utils import integer_to_amount
from .base import BasePriceStudyProduct
from .services import PriceStudyProductService


class PriceStudyProduct(BasePriceStudyProduct):
    """
    price study product
    """

    __tablename__ = "price_study_product"
    __table_args__ = default_table_args
    __mapper_args__ = {
        "polymorphic_on": "type_",
        "polymorphic_identity": __tablename__,
    }
    id = Column(
        ForeignKey("base_price_study_product.id", ondelete="CASCADE"), primary_key=True
    )
    base_sale_product_id = Column(
        Integer, ForeignKey("base_sale_product.id"), nullable=True
    )
    # Mode de calcul ht / supplier_ht
    mode = Column(String(20), default="ht", nullable=False)
    supplier_ht = Column(BigInteger(), default=0)

    # Relationships
    base_sale_product = relationship(
        "BaseSaleProduct",
        foreign_keys=[base_sale_product_id],
        info={"colanderalchemy": {"exclude": True}},
    )
    _endi_service = PriceStudyProductService

    def __json__(self, request):
        result = BasePriceStudyProduct.__json__(self, request)
        result.update(
            dict(
                base_sale_product_id=self.base_sale_product_id,
                supplier_ht=integer_to_amount(self.supplier_ht, 5, None),
                mode=self.mode,
            )
        )
        return result

    @classmethod
    def from_sale_product(cls, sale_product):
        instance = super(PriceStudyProduct, cls).from_sale_product(sale_product)
        instance.base_sale_product_id = sale_product.id
        instance.supplier_ht = getattr(sale_product, "supplier_ht", None)
        instance.mode = getattr(sale_product, "mode", "ht")
        return instance

    def duplicate(self):
        instance = BasePriceStudyProduct.duplicate(self)

        for field in ("supplier_ht", "base_sale_product_id", "mode"):
            setattr(instance, field, getattr(self, field, None))
        return instance
