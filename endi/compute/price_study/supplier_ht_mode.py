from endi.compute.sale_product import SaleProductSupplierHtComputer


class ProductSupplierHtComputer(SaleProductSupplierHtComputer):
    def _get_company(self):
        return self.product.get_company()

    def get_general_overhead(self):
        return self.product.get_general_overhead()

    def get_margin_rate(self):
        return self.product.margin_rate


class WorkItemSupplierHtComputer(ProductSupplierHtComputer):
    def _get_tva(self):
        return self.product.get_tva()

    def work_unit_flat_cost(self):
        value = self.flat_cost()
        quantity = self.product.work_unit_quantity
        if quantity is None:
            quantity = 0
        return quantity * value

    def full_flat_cost(self):
        value = self.flat_cost()
        quantity = self.product.total_quantity
        if quantity is None:
            quantity = 0
        return quantity * value

    def full_cost_price(self):
        value = self.cost_price()
        quantity = self.product.total_quantity
        if quantity is None:
            quantity = 0
        return quantity * value

    def full_intermediate_price(self):
        value = self.intermediate_price()
        quantity = self.product.total_quantity
        if quantity is None:
            quantity = 0
        return quantity * value
