from endi.compute.math_utils import compute_tva


class SaleProductSupplierHtComputer:
    """
    Computer class used to compute the values based on suplier_ht value

    flat_cost (supplier_ht) -> cost_price -> intermediate price -> unit_ht -> unit_ttc
    """

    def __init__(self, product):
        self.product = product
        self.company = self._get_company()
        self.tva = self._get_tva()

    def _get_company(self):
        return self.product.company

    def _get_tva(self):
        return self.product.tva

    def flat_cost(self):
        """
        Return the base cost of this sale product

        :returns: The result in 10^5 format
        :rtype: int
        """
        return self.product.supplier_ht or 0

    def get_general_overhead(self):
        return self.company.general_overhead

    def cost_price(self):
        """
        Compute the cost price of the given self.product

        :returns: The result in 10*5 format

        :rtype: int
        """
        overhead = self.get_general_overhead()
        if overhead is None:
            overhead = 0

        supplier_ht = self.flat_cost()
        if overhead != 0:
            result = supplier_ht * (1 + overhead)
        else:
            result = supplier_ht
        return result

    def get_margin_rate(self):
        return self.company.margin_rate

    def intermediate_price(self):
        """
        Compute the intermediate price of a work item

        3/    Prix intermédiaire = Prix de revient / ( 1 - ( Coefficients marge
        + aléas + risques ) )
        """
        margin_rate = self.get_margin_rate()
        if margin_rate is None:
            margin_rate = 0

        if margin_rate == 0:
            result = self.cost_price()
        elif margin_rate != 1:
            result = self.cost_price()
            result = result / (1 - margin_rate)
        else:
            result = 0
        return result

    def _get_contribution(self):
        from endi.models.company import Company

        return Company.get_contribution(self.company.id)

    def unit_ht(self, contribution=None):
        """
        Compute the ht value for the given work item
        """
        if contribution is None:
            contribution = self._get_contribution()

        intermediate_price = self.intermediate_price()
        result = intermediate_price
        if intermediate_price != 0:
            if isinstance(contribution, (int, float)):
                ratio = 1 - contribution / 100.0
                if ratio != 0:
                    result = intermediate_price / ratio
        else:
            result = self.product.ht or 0

        return result

    def unit_ttc(self):
        """
        Compute the ttc value for the given sale product
        """
        ht = self.unit_ht()
        tva = self.tva
        if tva is not None:
            return ht + compute_tva(ht, tva.value)
        else:
            return ht


class SaleProductWorkItemSupplierHtComputer(SaleProductSupplierHtComputer):
    def _get_company(self):
        return self.product.get_company()

    def _get_tva(self):
        return self.product.get_tva()

    def full_flat_cost(self):
        value = self.flat_cost()
        quantity = self.product.quantity
        if quantity is None:
            quantity = 0
        return quantity * value

    def full_cost_price(self):
        value = self.cost_price()
        quantity = self.product.quantity
        if quantity is None:
            quantity = 0
        return quantity * value

    def full_intermediate_price(self):
        value = self.intermediate_price()
        quantity = self.product.quantity
        if quantity is None:
            quantity = 0
        return quantity * value
