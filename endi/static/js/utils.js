/*

    Javascipt to handle utils interactions (menu, modals, ...)

*/


// UTILITY FUNCTIONS

function removeClass(el, cls) {
    var reg = new RegExp("(\\s|^)" + cls + "(\\s|$)");
    el.className = el.className.replace(reg, " ").replace(/(^\s*)|(\s*$)/g,"");
}

function addClass(el, cls) {
    el.className = el.className + " " + cls;
}

function hasClass(el, cls) {
    return el.className && new RegExp("(\\s|^)" + cls + "(\\s|$)").test(el.className);
}

function addEvent(obj, evType, fn){ 
    // from http://onlinetools.org/articles/unobtrusivejavascript/chapter4.html
    if (obj.addEventListener) { obj.addEventListener(evType, fn, false); return true; }
    if (obj.attachEvent) { var r = obj.attachEvent("on"+evType, fn); return r; }
    return false;
}


// COOKIES FUNCTIONS

function setCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function unsetCookie(name) {
    setCookie(name, "", -1);
}


// SHOW / HIDE FUNCTIONS (MENUS, MODALS, DROPDOWNS...)

function toggleOpen( whichObject, callingObject ) {
    var object = document.getElementById( whichObject );
    if ( !object ) object = whichObject;
    if ( hasClass( object, 'open' ) ) {
        removeClass( object, 'open' );
        if( callingObject ) {
            callingObject.title = callingObject.title.replace( 'Masquer' , 'Afficher' );
            callingObject.setAttribute( "aria-label", callingObject.title );
            callingObject.setAttribute( "aria-expanded", false );
        }
    } else {
        addClass( object, 'open' );
        if( callingObject ) {
            callingObject.title = callingObject.title.replace( 'Afficher' , 'Masquer' );
            callingObject.setAttribute( "aria-label", callingObject.title );
            callingObject.setAttribute( "aria-expanded", true );
        }
        var formElements = object.elements;
        if ( formElements ) formElements[1].focus();
    }
    return false;
}

function toggleMenu( callingObject ) {
    var callingButton = callingObject;
    var callingParent = callingButton.parentNode;
    var callingGrandParent = callingParent.parentNode;
    var callingGreatGrandParent = callingGrandParent.parentNode;
    var callingState = callingButton.getAttribute( 'aria-expanded' ).toString();
    var menuButtons = callingGrandParent.getElementsByTagName( 'BUTTON' );
    var menuListItems = callingGrandParent.getElementsByTagName( 'LI' );
    // 	close all siblings
    for ( i = 0; i < menuButtons.length; i ++) {
        menuButtons[i].setAttribute( 'aria-expanded' , 'false' );
        menuButtons[i].title = menuButtons[i].title.replace( 'Masquer' , 'Afficher' );
    }
    // expand clicked button submenu
    if ( callingState == 'false' ) {
        callingButton.setAttribute( 'aria-expanded' , 'true' );
        callingButton.title = callingButton.title.replace( 'Afficher' , 'Masquer' );
    }
    var currentClass = 'current_menu';
    if ( callingGreatGrandParent.tagName == "LI" ) {
        currentClass = 'current_submenu';
    }
    // remove current_submenu or current_menu class from all siblings
    for ( i = 0; i < menuListItems.length; i ++) {
        if ( hasClass( menuListItems[i], currentClass )) {
            removeClass( menuListItems[i], currentClass );
        }
    }
    // add current_subemnu or current_menu class to parent
    if ( callingState == 'false' ) {
        if ( !hasClass( callingParent, currentClass )) {
            addClass( callingParent, currentClass );
        }
    }
}

function resize( object, callingObject ) {
	var targetClass = object + "_mini";
	if( hasClass( document.body, targetClass )) {
		removeClass( document.body, targetClass );
		if( callingObject ) {
			callingObject.title = callingObject.title.replace( 'Afficher' , 'Réduire' );
            callingObject.setAttribute( "aria-label", callingObject.title );
		}
	}
	else {
		addClass( document.body, targetClass );
		if( callingObject ) {
			callingObject.title = callingObject.title.replace( 'Réduire' , 'Afficher' );
            callingObject.setAttribute( "aria-label", callingObject.title );
		}
    }
    document.body.offsetHeight;
    setCookie('endi__menu_mini', hasClass(document.body, targetClass));
}

function toggleModal( whichModal ){
	var modalObject = document.getElementById( whichModal );
	var modalButtons = modalObject.getElementsByTagName( 'BUTTON' );
	if ( modalObject.style.display == 'none' ){
		modalObject.setAttribute( 'style', 'display:flex;' );
		removeClass( modalObject, 'dismiss' );
		addClass( document.body, 'modal_open' );
		addClass( modalObject, 'appear' );
		for ( i=0 ; i < modalButtons.length ; i++ ) {
			if ( hasClass( modalButtons[i], 'main' ) ) modalButtons[i].focus();
		}
        autofocusContent(modalObject);
	}
	else {
		modalObject.setAttribute( 'style', 'display:none;' );
		removeClass( document.body, 'modal_open' );
		removeClass( modalObject, 'appear' );
		addClass( modalObject, 'dismiss' );
	}
}
